var assert = require('chai').assert,
    expect = require('chai').expect,
    arrayList = require('../index.js').arrayList;

describe("homework functions", function() {
    describe('function arrayList', function(){
        let arr = new arrayList;

        it('arrayList testing function init', function(){
            expect(arr.init([1, 2, 3, 4, 5])).to.eql([1, 2, 3, 4, 5]);
        });

        it('arrayList testing function getSize', function(){
            assert.equal(arr.getSize(), 5)
        });

        it('arrayList testing function push', function(){
            expect(arr.push(6)).to.eql([1, 2, 3, 4, 5, 6]);
        });

        it('arrayList testing function toString', function(){
            assert.equal(arr.toString(), "123456")
        });

        it('arrayList testing function pop', function(){
            assert.equal(arr.pop(), 6)
        });

        it('arrayList testing function shift', function(){
            assert.equal(arr.shift(), 1)
        });

        it('arrayList testing function unshift', function(){
            expect(arr.unshift("Привет", "Пока")).to.eql(["Привет", "Пока", 2, 3, 4, 5]);
        });

        it('arrayList testing function slice', function(){
            expect(arr.slice(0, 4)).to.eql(["Привет", "Пока", 2, 3]);
        });

        it('arrayList testing function splice', function(){
            expect(arr.splice(1, 2, 3)).to.eql(['Привет', 3, 3, 4, 5]);
        });

        it('arrayList testing function sort', function(){
            expect(arr.sort()).to.eql(['Привет', 3, 3, 4, 5]);
        });

        it('arrayList testing function get', function(){
            assert.equal(arr.get(2), 3)
        });

        it('arrayList testing function set', function(){
            expect(arr.set(1, 2)).to.eql(['Привет', 2, 3, 4, 5]);
        });
    });
});
