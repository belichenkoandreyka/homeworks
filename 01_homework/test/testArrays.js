var assert = require('chai').assert,
    findMinElement = require('../script/scriptArrays.js').findMinElement,
    findMaxElement = require('../script/scriptArrays.js').findMaxElement,
    findMinIndex = require('../script/scriptArrays.js').findMinIndex,
    findMaxIndex = require('../script/scriptArrays.js').findMaxIndex,
    sumOfOddIndex = require('../script/scriptArrays.js').sumOfOddIndex,
    countOddElements = require('../script/scriptArrays.js').countOddElements,
    revArray = require('../script/scriptArrays.js').revArray,
    changePlaces = require('../script/scriptArrays.js').changePlaces,
    bubble = require('../script/scriptArrays.js').bubble,
    select = require('../script/scriptArrays.js').select,
    insertArray = require('../script/scriptArrays.js').insertArray;

describe("homework3 linear arrays", function() {

    //1)Минимальный елемент массива

    describe("function findMinElement", function() {
        it("should return the min element of array [1, -5, -1, 0, 9] that is -5", function () {
            assert.equal(findMinElement([1, -5, -1, 0, 9]), -5);
        });
        it("should return the min element of array [1, 2, 3, 4, 5] that is 1", function () {
            assert.equal(findMinElement([1, 2, 3, 4, 5]), 1);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(findMinElement([]), 0);
        });
        it("should return error of function findMinElement", function() {
            assert.equal(findMinElement('Привет'), void 0);
        });
    });

    //2) Найти максимальный елемент массива

    describe("function findMaxElement", function() {
        it("should return the max element of array [1, -5, 9, -1, 0] that is 9", function () {
            assert.equal(findMaxElement([1, -5, 9, -1, 0]), 9);
        });
        it("should return the max element of array [1, 2, 3, 4, 5] that is 5", function () {
            assert.equal(findMaxElement([1, 2, 3, 4, 5]), 5);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(findMaxElement([]), 0);
        });
        it("should return error of function findMinElement", function() {
            assert.equal(findMaxElement(123), void 0);
        });
    });

    //3)Найти индекс минимального элемента в массиве

    describe("function findMinIndex", function() {
        it("should return the index of min element of array [1, -5, 9, -1, 0] that is 1", function () {
            assert.equal(findMinIndex([1, -5, 9, -1, 0]), 1);
        });
        it("should return the index of min element of array [1, 2, 3, 4, 5] that is 0", function () {
            assert.equal(findMinIndex([1, 2, 3, 4, 5]), 0);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(findMinIndex([]), 0);
        });
        it("should return error of function findMinIndex", function() {
            assert.equal(findMinIndex(123), void 0);
        });
    });

    //4)Найти индекс максимального элемента в массиве

    describe("function findMaxIndex", function() {
        it("should return the index of max element of array [1, -5, 9, -1, 0] that is 2", function () {
            assert.equal(findMaxIndex([1, -5, 9, -1, 0]), 2);
        });
        it("should return the index of max element of array [5, 2, 3, 4, 1] that is 0", function () {
            assert.equal(findMaxIndex([5, 2, 3, 4, 1]), 0);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(findMaxIndex([]), 0);
        });
        it("should return error of function findMaxIndex", function() {
            assert.equal(findMaxIndex(123), void 0);
        });
    });

    //5) Посчитать сумму елементов массива с нечетными индексами

    describe("function sumOfOddIndex", function() {
        it("should return the sum of element of array with odd index [1, -5, 9, -1, 0] that is -6", function () {
            assert.equal(sumOfOddIndex([1, -5, 9, -1, 0]), -6);
        });
        it("should return the sum of element of array with odd index [5, 2, 3, 4, 1] that is 6", function () {
            assert.equal(sumOfOddIndex([5, 2, 3, 4, 1]), 6);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(sumOfOddIndex([]), 0);
        });
        it("should return error of function sumOfOddIndex", function() {
            assert.equal(sumOfOddIndex(true), void 0);
        });
    });

    //6) Сделать реверс массива

    describe("function revArray", function() {
        it("should return to revers of the array [1, -5, 9, -1]", function () {
            assert.isArray(revArray([1, -5, 9, -1]), [-1, 9, -5, 1]);
        });
        it("should return to revers of the array [5, 2, 3, 4]", function () {
            assert.isArray(revArray([5, 2, 3, 4]), [4, 3, 2, 5]);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(revArray([]), 0);
        });
        it("should return error of function revArray", function() {
            assert.equal(revArray(''), void 0);
        });
    });

    //7) Посчитать количество нечетных елементов в массиве.

    describe("function countOddElements", function() {
        it("should return quantity of odd elements of array [1, -5, 9, -1, 0] that is 4", function () {
            assert.equal(countOddElements([1, -5, 9, -1, 0]), 4);
        });
        it("should return quantity of odd elements of array [5, 2, 3, 4, 1] that is 3", function () {
            assert.equal(countOddElements([5, 2, 3, 4, 1]), 3);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(countOddElements([]), 0);
        });
        it("should return error of function countOddElements", function() {
            assert.equal(countOddElements(123), void 0);
        });
    });


    //8)Поменять местами первую и вторую половину массива,например для массива 1234 результат 3412.

    describe("function changePlaces", function() {
        it("should return to swap 2 parts of the array [1, -5, 9, -1, 0, 6]", function () {
            assert.isArray(changePlaces([1, -5, 9, -1, 0, 6]), [-1, 0, 6, 1, -5, 9]);
        });
        it("should return to swap 2 parts of the array [5, 2, 3, 4]", function () {
            assert.isArray(changePlaces([5, 2, 3, 4]), [3, 4, 5, 2]);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(changePlaces([]), 0);
        });
        it("should return error of function changePlaces", function() {
            assert.equal(changePlaces(''), void 0);
        });
    });

    //9) Отсортировать массив(пузырьком(Bubble), выбором(Select), вставками(Insert)).

    describe("function bubble", function() {
        it("should return an array[5, -1, 3, 7, 2, 1] in ascending order", function () {
            assert.isArray(bubble([5, -1, 3, 7, 2, 1]), [7, 5, 3, 2, 1, -1]);
        });
        it("should return an array [-5, -4, -3, -2, -1] in ascending order", function () {
            assert.isArray(bubble([-5, -4, -3, -2, -1]), [-1, -2, -3, -4, -5]);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(bubble([]), 0);
        });
        it("should return error of function bubble", function() {
            assert.equal(bubble(123), void 0);
        });
    });

    //Сортировка выбором

    describe("function select", function() {
        it("should return an array[5, -1, 3, 7, 2, 1] in ascending order", function () {
            assert.isArray(select([5, -1, 3, 7, 2, 1]), [7, 5, 3, 2, 1, -1]);
        });
        it("should return an array [-5, -4, -3, -2, -1] in ascending order", function () {
            assert.isArray(select([-5, -4, -3, -2, -1]), [-1, -2, -3, -4, -5]);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(select([]), 0);
        });
        it("should return error of function select", function() {
            assert.equal(select(123), void 0);
        });
    });

    //Сортировка вставками

    describe("function insertArray", function() {
        it("should return an array[5, -1, 3, 7, 2, 1] in ascending order", function () {
            assert.isArray(insertArray([5, -1, 3, 7, 2, 1]), [7, 5, 3, 2, 1, -1]);
        });
        it("should return an array [-5, -4, -3, -2, -1] in ascending order", function () {
            assert.isArray(insertArray([-5, -4, -3, -2, -1]), [-1, -2, -3, -4, -5]);
        });
        it("should return 0 because the array is empty", function () {
            assert.equal(insertArray([]), 0);
        });
        it("should return error of function insertArray", function() {
            assert.equal(insertArray(''), void 0);
        });

    });

});