var assert = require('chai').assert,
    nameOfDay = require('../script/scriptFunctions.js').nameOfDay,
    nameOfNumber = require('../script/scriptFunctions.js').nameOfNumber,
    distanceBetweenPoints = require('../script/scriptFunctions.js').distanceBetweenPoints;

describe("homework functions", function() {
//1) Получить строковое значение дня недели по номеру дня

    describe('function nameOfDays', function(){
        it('should return string name of number day Thursday', function(){
            assert.equal(nameOfDay(2), 'Вторник')
        });
        it('should return string name of number day Friday', function(){
            assert.equal(nameOfDay(5), 'Пятница')
        });
        it('should return error of function nameofDay', function(){
            assert.equal(nameOfDay(''), void 0);
        });
    });

//2) найти расстояние между точками двумя точками в двумерном декартовом пространстве

    describe('function distanceBetweenPoints', function(){
        it('should return distance between point [1; 2] and [3; 4] that is 3', function(){
            assert.equal(distanceBetweenPoints(1, 2, 3, 4), 3)
        });
        it('should return distance between point [2; 3] and [4; 5] that is 3', function(){
            assert.equal(distanceBetweenPoints(2, 3, 4, 5), 3)
        });
        it('should return error of function distanceBetweenPoints', function(){
            assert.equal(distanceBetweenPoints(''), void 0);
        });
    });

//3) Вводим число(0-999), получаем строку с прописью числа;

    describe('function nameOfNumber', function(){
        it('should return string name of number one hundred fifty', function(){
            assert.equal(nameOfNumber(150), 'One Hundred Fifty ')
        });
        it('should return string name of number nine hundred ninety nine', function(){
            assert.equal(nameOfNumber(999), 'Nine Hundreds Ninety Nine')
        });
        it('should return string name of number five hundred', function(){
            assert.equal(nameOfNumber(500), 'Five Hundreds ');
        });
    });
});