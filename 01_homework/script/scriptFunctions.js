module.exports =  {
//ФУНКЦИИ
//1) Получить строковое значение дня недели по номеру дня

nameOfDay: function(number){
    if (typeof number === "number") {
        var days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресен'];
        var result;

        if (number >= 1 || number <= 7) {
            result = days[number - 1];
        }

        return result;
    }
},

//2) Найти расстояние между двумя точками в двумерном декартовом пространстве.

distanceBetweenPoints: function(x1, y1, x2, y2){
    if (typeof x1 === "number" && typeof x2 === "number" && typeof y1 === "number" && typeof y2 === "number") {
        return Math.round(Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2));
    }
},

//3) Вводим число(0-999), получаем строку с прописью числа;

    nameOfNumber: function call(number) {
        if (typeof number === 'number') {
            if (number === 0) {
                return '';
            }else if (number <= 19) {
                var arr19 = ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight',
                    'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen',
                    'Seventeen', 'Eighteen', 'Nineteen'];
                return arr19[number - 1];
            }else if (number <= 99) {
                var arrDozens = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy',
                    'Eighty', 'Ninety'];
                return arrDozens[Math.floor(number / 10 - 2)] + ' ' + call(number % 10);
            }else if (number <= 199) {
                return 'One Hundred ' + call(number % 100);
            }else if (number <= 999) {
                return call(Math.floor(number / 100)) + ' Hundreds ' + call(number % 100);
            }
        }

    },


    // nameOfNumber: function(number){
    //      var objOfNumber = {
    //         1: 'one',
    //         2: 'two',
    //         3: 'three',
    //         4: 'four',
    //         5: 'five',
    //         6: 'six',
    //         7: 'seven',
    //         8: 'eight',
    //         9: 'nine',
    //         10: 'ten',
    //         11: 'eleven',
    //         12: 'twelve',
    //     };
    //
    //     var unusualNameOfNumber = {
    //         2: 'twen',
    //         3: 'thir',
    //         5: 'fif',
    //     };
    //
    //     let template = [];
    //     let count = 0;
    //     let word = '';
    //     let nominalNumber = number;
    //     while(number > 0) {
    //         let b = number % 10;
    //         number -= b;
    //         number /= 10;
    //         template.unshift(b);
    //         count++;
    //     }
    //     if(count === 1){
    //         word += objOfNumber[template[0]];
    //     }else if(count === 2){
    //         if(template[1] < 3 && template[1] > -1 && template[0] === 1){ //10-12
    //             word += objOfNumber[nominalNumber];
    //         }else if(template[0] === 2 ||
    //             template[0] === 3 ||
    //             template[0] === 5){
    //
    //             if(template[1] > 0){
    //                 word += unusualNameOfNumber[template[0]] + 'ty ' + objOfNumber[template[1]];
    //             }else if(template[1] <= 0){
    //                 word += unusualNameOfNumber[template[0]] + 'ty';
    //             }
    //
    //         }else{
    //
    //             if(template[1] > 0){
    //                 word += objOfNumber[template[0]] + 'ty ' + objOfNumber[template[1]];
    //             }else if(template[1] <= 0){
    //                 word += objOfNumber[template[0]] + 'ty';
    //             }
    //
    //         }
    //     }else if(count === 3){
    //         if(template[2] < 3 && template[2] > -1 && template[1] === 1){ //110-112
    //             for(let i in objOfNumber){
    //                 if(i % 10 === template[2] && i / 10 >= 1) {
    //                     word += objOfNumber[template[0]] + ' hundred ' + objOfNumber[i];
    //                 }
    //             }
    //         }else if(template[2] > 2 && template[1] === 1){
    //             if(template[2] === 4 || template[2] > 5) {
    //                 word += objOfNumber[template[0]] + ' hundred ' + objOfNumber[template[2]] + 'teen';
    //             }else if(template[2] === 3 || template[2] === 5){
    //                 word += objOfNumber[template[0]] + ' hundred ' + unusualNameOfNumber[template[2]] + 'teen';
    //             }
    //         }else if(template[1] === 2 ||
    //             template[1] === 3 ||
    //             template[1] === 5){
    //
    //             if(template[2] > 0){
    //                 word += objOfNumber[template[0]] + ' hundred ' + unusualNameOfNumber[template[1]] + 'ty ' + objOfNumber[template[2]];
    //             }else if(template[2] <= 0){
    //                 word += objOfNumber[template[0]] + ' hundred ' + unusualNameOfNumber[template[1]] + 'ty';
    //             }
    //
    //         }else if(template[1] === 0 && template[2] !== 0){
    //             word += objOfNumber[template[0]] + ' hundred ' + objOfNumber[template[2]]
    //         }else if(template[1] === 0 && template[2] === 0){
    //             word += objOfNumber[template[0]] + ' hundred';
    //         }else{
    //
    //             if(template[2] > 0){
    //                 word += objOfNumber[template[0]] + ' hundred ' + objOfNumber[template[1]] + 'ty ' + objOfNumber[template[2]];
    //             }else{
    //                 word += objOfNumber[template[0]] + ' hundred ' + objOfNumber[template[1]] + 'ty';
    //             }
    //
    //         }
    //     }
    //     return word;
    // }
};