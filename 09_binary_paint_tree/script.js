let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');

let inputArray = document.getElementById('getArray');
let drawBtn = document.getElementById('drawBtn');
// let xFrom = 300;
// let yFrom = 100;

let BSTree= function () {
    this.root = null;
};
let Node = function(value, x, y){
    this.value = value;
    this.right = null;
    this.left = null;
    this.x = x;
    this.y = y;
};

BSTree.prototype.init = function(array){

    for (let  i = 0; i < array.length; i++){
        this.add(array[i]);
    }
    this.drawArrows(this.root);
};

BSTree.prototype.drawArrows = function(node){
    // console.log(node);
    this.root = drawArr(this.root);
};

function drawArr(node){
    if(node.right !== null){
        ctx.beginPath();
        ctx.moveTo(node.x, node.y);
        ctx.lineTo(node.right.x, node.right.y);
        ctx.stroke();
        drawArr(node.right);
    }if(node.left !== null){
        ctx.beginPath();
        ctx.moveTo(node.x, node.y);
        ctx.lineTo(node.left.x, node.left.y);
        ctx.stroke();
        drawArr(node.left);
    }
}


BSTree.prototype.add = function(value) {
    if (value === null) {
        return;
    }
    this.root = addNode(this.root, value);
};


function addNode (node, value, x = 300, y = 100) {
    if (node === null) {
        ctx.beginPath();
        ctx.arc(x, y, 20, 0, Math.PI * 2);
        ctx.font = "18px Verdana";
        ctx.fillText(value, x-5, y+5);
        ctx.stroke();
        node = new Node(value, x, y);
    }
    else if (value < node.value) {
        x = x - 100;
        y = y + 50;

        node.left = addNode(node.left, value, x, y);
    } else {
        x = x + 100;
        y = y + 50;
        node.right = addNode(node.right, value, x, y);
    }
    return node;
}

let array = [];
drawBtn.addEventListener('click', getArray);

function getArray(){
    array = inputArray.value.split(', ');
    array.forEach(function(item, index, array){
        array[index] = Number(item);
    });
    let tree = new BSTree();
    // console.log(array);
    tree.init(array);
}