const bodyParser = require('body-parser');
const express = require('express');
const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: true}));

server.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

server.listen(3000, function () {
    console.log('Server started on port 3000..')
});

server.post('/javascript', function (req, res) {
    let currents =  req.body.firstNumber;
    let action = req.body.action;
    let currentValue = req.body.secondNumber;

    if ('plus' === action){
       currents += currentValue;
    }
    else if ('minus' === action) {
        currents -= parseFloat(currentValue);
    }
    else if ('devide' === action) {
        currents /= parseFloat(currentValue);
    } else if ('multiple' === action) {
        currents *= parseFloat(currentValue);
    }
    else{
        currents = parseFloat(currentValue);
    }
    res.status(200).send(currents.toString());
});
